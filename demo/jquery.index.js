import $ from 'jquery'
import '../jquery' // eslint-disable-line import/no-unassigned-import

import {PLUGIN_NAME} from '../src'
import styles from './main.css'

const $peek = $('.peekaboo')[PLUGIN_NAME]({
  classNames: {
    isLoading: styles.loading,
    hasLoaded: styles.loaded,
    hasError: styles.error,
    isOnscreen: styles.onscreen,
    isOffscreen: styles.offscreen,
    isAnimating: styles.animating,
    isDone: styles.done,
    pluginLoaded: styles.init,
    initiallyVisible: styles.visible
  },
  preload: true,
  offset: 250
})

$('.destroy').on('click', () => {
  $peek.each((i, $p) => $p.destroy())
})
