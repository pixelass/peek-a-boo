import Peekaboo from '../src'
import {loadImages} from '../src/helpers'
import styles from './main.css'

const images = Array.from(global.document.querySelectorAll('.peekaboo img')).map(el => el.src)
const elements = Array.from(global.document.querySelectorAll('.peekaboo'))

const loading = () => {
  const wrapper = global.document.createElement('div')
  wrapper.className = styles.spinner
  wrapper.innerHTML = `
    <div>Loading</div>
  `
  return wrapper
}
const spinner = loading()
global.document.body.appendChild(spinner)

elements.forEach(el => el.classList.add(styles.hidden))
loadImages(images).then(() => {
  spinner.remove()
  const peeks = elements.map(el => {
    const peek = new Peekaboo({
      classNames: {
        isLoading: styles.loading,
        hasLoaded: styles.loaded,
        hasError: styles.error,
        isOnscreen: styles.onscreen,
        isOffscreen: styles.offscreen,
        isAnimating: styles.animating,
        isDone: styles.done,
        pluginLoaded: styles.init,
        initiallyVisible: styles.visible
      },
      preload: true,
      offset: 250
    })
    peek.init(el)
    return peek
  })

  global.document.querySelector('.destroy').addEventListener('click', () =>
    peeks.forEach(peek => peek.destroy())
  )
})
