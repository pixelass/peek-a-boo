import Peekaboo from '../'
import styles from './main.css'

const elements = Array.from(global.document.querySelectorAll('.peekaboo'))

const peeks = elements.map(el => {
  const peek = new Peekaboo({
    classNames: {
      isLoading: styles.loading,
      hasLoaded: styles.loaded,
      hasError: styles.error,
      isOnscreen: styles.onscreen,
      isOffscreen: styles.offscreen,
      isAnimating: styles.animating,
      isDone: styles.done,
      pluginLoaded: styles.init,
      initiallyVisible: styles.visible
    },
    preload: true,
    offset: 250
  })
  peek.init(el)
  return peek
})

global.document.querySelector('.destroy').addEventListener('click', () =>
  peeks.forEach(peek => peek.destroy())
)
