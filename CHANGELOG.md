# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.0.5"></a>
## [1.0.5](https://github.com/pixelass/peek-a-boo/compare/v1.0.3...v1.0.5) (2017-05-16)


### Bug Fixes

* **npm:** better exports ([48597bc](https://github.com/pixelass/peek-a-boo/commit/48597bc))
* **npm import:** use root files to easy-import ([c51225b](https://github.com/pixelass/peek-a-boo/commit/c51225b))



<a name="1.0.3"></a>
## [1.0.3](https://github.com/pixelass/peek-a-boo/compare/v1.0.2...v1.0.3) (2017-05-12)


### Bug Fixes

* **npm:** added directories ([8dad7d6](https://github.com/pixelass/peek-a-boo/commit/8dad7d6))



<a name="1.0.2"></a>
## [1.0.2](https://github.com/pixelass/peek-a-boo/compare/v1.0.1...v1.0.2) (2017-05-12)


### Bug Fixes

* **npm:** folders -> files ? ([0a9828e](https://github.com/pixelass/peek-a-boo/commit/0a9828e))



<a name="1.0.1"></a>
## [1.0.1](https://github.com/pixelass/peek-a-boo/compare/v1.0.0...v1.0.1) (2017-05-12)


### Bug Fixes

* **npm:** allow import of jquery ([680b890](https://github.com/pixelass/peek-a-boo/commit/680b890))



<a name="1.0.0"></a>
# 1.0.0 (2017-05-11)


### Bug Fixes

* **methods:** correctly destroy ([0d4cb11](https://github.com/pixelass/peek-a-boo/commit/0d4cb11))


### Features

* **methods:** added destroy ([742bcde](https://github.com/pixelass/peek-a-boo/commit/742bcde))
* **package:** first dev version ([a7acd13](https://github.com/pixelass/peek-a-boo/commit/a7acd13))
