/*
 * if you want to use css-modules or cssnext
 * please follow the comments in this file.
 *
 * css-modules
 * (1): import css-modulesify (requires `yarn add css-modulesify`)
 * (2): remove css files from copy batch (comment or delete)
 * (3): load css-modulsify into browserify
 *
 * cssnext
 * (4): import cssnext (requires `yarn add postcss-cssnext`)
 * (5): run cssnext in the "after" batch
 */

const fs = require('fs')
const path = require('path')
const Log = require('log')

const log = new Log('info')

const rootFolder = path.join(__dirname, '../')
const libFolder = '../lib'

const exportMap = {
  'index.js': 'index',
  'jquery/index.js': 'jquery',
  'helpers.js': 'helpers'
}

const getModuleContent = file => `/**
 * This file was generated for npm.
 * @file ./lib/${file}
 * @module ${file}
 */

module.exports = require("./lib/${file}");
`


Object.keys(exportMap).forEach(file => {
  const filePath = path.join(rootFolder, `${exportMap[file]}.js`)
  fs.writeFile(filePath, getModuleContent(file), err => {
    if (err) {
      throw err
    } else {
      log.info(`Wrote file ${filePath}`)
    }
  })
})
