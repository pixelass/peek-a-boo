(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
'use strict';

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _main = require('./main.css');

var _main2 = _interopRequireDefault(_main);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var elements = Array.from(global.document.querySelectorAll('.peekaboo'));

var peeks = elements.map(function (el) {
  var peek = new _2.default({
    classNames: {
      isLoading: _main2.default.loading,
      hasLoaded: _main2.default.loaded,
      hasError: _main2.default.error,
      isOnscreen: _main2.default.onscreen,
      isOffscreen: _main2.default.offscreen,
      isAnimating: _main2.default.animating,
      isDone: _main2.default.done,
      pluginLoaded: _main2.default.init,
      initiallyVisible: _main2.default.visible
    },
    preload: true,
    offset: 250
  });
  peek.init(el);
  return peek;
});

global.document.querySelector('.destroy').addEventListener('click', function () {
  return peeks.forEach(function (peek) {
    return peek.destroy();
  });
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"../":3,"./main.css":2}],2:[function(require,module,exports){
module.exports = {"empty-ani":"_empty-ani_ta8ho_1","show-me":"_show-me_ta8ho_1","init":"_init_ta8ho_38","hidden":"_hidden_ta8ho_39","onscreen":"_onscreen_ta8ho_77","visible":"_visible_ta8ho_106","offscreen":"_offscreen_ta8ho_114","done":"_done_ta8ho_122","error":"_error_ta8ho_137","loading":"_loading_ta8ho_138","loaded":"_loaded_ta8ho_139","animating":"_animating_ta8ho_140","spinner":"_spinner_ta8ho_156"}
},{}],3:[function(require,module,exports){
"use strict";

/**
 * This file was generated for npm.
 * @file ./lib/index.js
 * @module index.js
 */

module.exports = require("./lib/index.js");

},{"./lib/index.js":5}],4:[function(require,module,exports){
(function (global){
'use strict';

var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
};

/**
 * Small helper library. Includes image preloaders and viewport checks
 * @file src/heplers.js
 * @module helpers
 * @author( Gregor Adams <greg@pixelass.com>)
 */

/**
 * Load image with a promise
 * @param {string} src
 * @returns {promise}
 */
var loadImage = function loadImage(src) {
  return new Promise(function (resolve, reject) {
    if (typeof src === 'string') {
      var img = global.document.createElement('img');
      img.src = src;
      img.onload = function (e) {
        return resolve(e.target);
      };
      img.onerror = function () {
        return resolve(null);
      };
      return img;
    }
    return reject(new Error('Expected src to be of type "string". instead got ' + (typeof src === 'undefined' ? 'undefined' : _typeof(src))));
  });
};

/**
 * Loads a set of images with a promise
 * @param {array.<string>} arr
 * @returns {promise}
 */
var loadImages = function loadImages(sources) {
  return Promise.all(sources.map(function (src) {
    return loadImage(src);
  }));
};

/**
 * Check if an element has entered the page from the bottom.
 * Respects offset if set. The offset will define how far the image has entered the screen.
 * @param {HTMLElement} el
 * @param {number} offset Number of pixels the image must have entered the viewport.
 *                        Can be a positive or negative number.
 * @returns {boolean} returns true if the image is visible
 */
var inViewport = function inViewport(element) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

  var _element$getBoundingC = element.getBoundingClientRect(),
      top = _element$getBoundingC.top;

  var offsetBottom = top - global.innerHeight + offset;
  return offsetBottom < 0;
};

exports.loadImage = loadImage;
exports.loadImages = loadImages;
exports.inViewport = inViewport;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PLUGIN_NAME = undefined;

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}(); /**
      * @file src/index.js
      * @module Peekaboo
      * @author( Gregor Adams <greg@pixelass.com>)
      */

var _oneListener = require('one-listener');

var _oneListener2 = _interopRequireDefault(_oneListener);

var _helpers = require('./helpers');

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _toConsumableArray(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }return arr2;
  } else {
    return Array.from(arr);
  }
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var throttled = new _oneListener2.default({ limit: 0, throttle: 250 });

/**
 * Global name of the jQuery plugin. This name will be used to call the plugin
 *
 * @type {string}
 * @example
 * $('.selector')[PLUGIN_NAME]()
 * $('.selector').Peekaboo()
 */
var PLUGIN_NAME = 'Peekaboo';

/**
 * Default options for plugin instances
 * @type {object}
 * @prop {object} classNames
 * @prop {boolean} preload
 * @prop {number} offset
 */
var PLUGIN_DEFAULTS = {
  classNames: {
    isLoading: 'loading',
    hasLoaded: 'loaded',
    hasError: 'error',
    isOnscreen: 'onscreen',
    isOffscreen: 'offscreen',
    isAnimating: 'animating',
    isDone: 'done',
    initiallyVisible: 'visible',
    pluginLoaded: 'peekabooLoaded'
  },
  preload: false,
  delay: false,
  offset: 150
};

var Peekaboo = function () {
  /**
   * @param {object} [options={}]
   * @returns {this}
   */
  function Peekaboo() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, Peekaboo);

    this.options = _extends({}, PLUGIN_DEFAULTS, options);
    this.events = ['scroll', 'resize'];
    this.addAnimationClasses = this.addAnimationClasses.bind(this);
    this.removeAnimationClasses = this.removeAnimationClasses.bind(this);
  }

  /**
   * Initialize plugin
   * @param {HTMLElement} el
   * @returns {undefined}
   */

  _createClass(Peekaboo, [{
    key: 'init',
    value: function init(el) {
      var _this = this;

      var pluginLoaded = this.options.classNames.pluginLoaded;

      this.el = el;
      el.classList.add(pluginLoaded);
      var nestedImages = Array.from(el.querySelectorAll('img'));
      if (this.options.preload) {
        return (0, _helpers.loadImages)(nestedImages.map(function (el) {
          return el.src;
        })).then(function () {
          return _this.onInit(el);
        }).catch(function (err) {
          throw err;
        });
      }
      return this.onInit(el);
    }

    /**
     * Destroy instance.
     * Removes all listeners and classes
     */

  }, {
    key: 'destroy',
    value: function destroy() {
      var _this2 = this,
          _el$classList;

      var classNames = this.options.classNames;

      var classes = Object.keys(classNames).map(function (key) {
        return classNames[key];
      });
      this.events.forEach(function (event) {
        _this2['cancel_' + event]();
      });
      this.el.removeEventListener('animationstart', this.addAnimationClasses);
      this.el.removeEventListener('animationend', this.removeAnimationClasses);
      (_el$classList = this.el.classList).remove.apply(_el$classList, _toConsumableArray(classes));
    }

    /**
     * Callback ofter plugin instance has been initialized
     * @param {HTMLElement} el
     */

  }, {
    key: 'onInit',
    value: function onInit(el) {
      var _this3 = this;

      var _options$classNames = this.options.classNames,
          hasLoaded = _options$classNames.hasLoaded,
          isLoading = _options$classNames.isLoading;
      // Create namespaced eventnames

      var initiallyVisible = (0, _helpers.inViewport)(el, this.options.offset);
      this.initClasses(el, initiallyVisible);

      this.events.forEach(function (event) {
        _this3['cancel_' + event] = throttled.requestEventListener(event, function () {
          _this3.checkForItems(el).then(function () {
            _this3['cancel_' + event]();
          }).catch(function (err) {
            return err;
          });
        });
      });

      // Add eventlisteners for the animation once
      // When done add a class as a flag

      el.addEventListener('animationstart', this.addAnimationClasses, {
        passive: true,
        once: true
      });
      el.addEventListener('animationend', this.removeAnimationClasses, {
        passive: true,
        once: true
      });
      // Images are loaded.
      // Toggle the classes
      el.classList.add(hasLoaded);
      el.classList.remove(isLoading);
    }
  }, {
    key: 'addAnimationClasses',
    value: function addAnimationClasses() {
      var isAnimating = this.options.classNames.isAnimating;

      this.el.classList.add(isAnimating);
    }
  }, {
    key: 'removeAnimationClasses',
    value: function removeAnimationClasses() {
      var _options$classNames2 = this.options.classNames,
          isAnimating = _options$classNames2.isAnimating,
          isDone = _options$classNames2.isDone;

      this.el.classList.remove(isAnimating);
      this.el.classList.add(isDone);
    }

    /**
     * Handles initially needed classNames on an element
     * @param {HTMLElement} el
     * @param {boolean} visible
     */

  }, {
    key: 'initClasses',
    value: function initClasses(element, visible) {
      var _options$classNames3 = this.options.classNames,
          isLoading = _options$classNames3.isLoading,
          initiallyVisible = _options$classNames3.initiallyVisible;

      element.classList.add(isLoading);
      element.classList.toggle(initiallyVisible, visible);
      this.currentClasses(element, visible);
    }

    /**
     * add live classNames. Adds `isOnscreen` or `isOffscreen` class depending on the visibility
     * @param {HTMLElement} el
     * @param {boolean} visible
     */

  }, {
    key: 'currentClasses',
    value: function currentClasses(element, visible) {
      var _options$classNames4 = this.options.classNames,
          isOffscreen = _options$classNames4.isOffscreen,
          isOnscreen = _options$classNames4.isOnscreen;

      element.classList.toggle(isOnscreen, visible);
      element.classList.toggle(isOffscreen, !visible);
    }

    /**
     * @param {HTMLElement} el
     * @returns {promise}
     */

  }, {
    key: 'checkForItems',
    value: function checkForItems(el) {
      var _this4 = this;

      return new Promise(function (resolve, reject) {
        var isVisible = (0, _helpers.inViewport)(el, _this4.options.offset);
        _this4.currentClasses(el, isVisible);
        if (isVisible) {
          resolve(el);
        } else {
          reject(new Error('Element not visible'));
        }
      });
    }
  }]);

  return Peekaboo;
}();

exports.PLUGIN_NAME = PLUGIN_NAME;
exports.default = Peekaboo;

},{"./helpers":4,"one-listener":6}],6:[function(require,module,exports){
'use strict';

module.exports = require('./lib/');

},{"./lib/":10}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _throttle = require('throttle-debounce/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * add the propper event listener to `window`
 * @type {Function}
 * @const
 * @private
 * @param  {String} event - event to listen to
 * @param  {Object} handler - the handler object containing the throttled and unthrottled handlers
 * @param  {Function} handler.default - default handler
 * @param  {Function} handler.throttled - throttled handler
 * @param  {Number} delay - throttle in milliseconds
 * @return {Object} returns the new handlers
 */
var addEventListeners = function addEventListeners(event, handler) {
  var delay = arguments.length <= 2 || arguments[2] === undefined ? 0 : arguments[2];

  if (delay > 0) {
    handler.throttled = (0, _throttle2.default)(delay, function (e) {
      return handler.default(e);
    });
    window.addEventListener(event, handler.throttled);
  } else {
    window.addEventListener(event, handler.default);
  }
  return handler;
}; /**
    * Helper function to add eventListeners to `window`
    * @module  addEventListeners
    * @private
    * @author  Gregor Adams  <greg@pixelass.com>
    */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 * @author niksy
 * @private
 * @type {Function}
 * @const
 * @param  {Number} delay - A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250
 *                        (or even higher) are most useful.
 * @param  {Boolean} noTrailing - Optional, defaults to false. If noTrailing is true, callback will only execute every
 *                              `delay` milliseconds while the throttled-function is being called. If noTrailing is false
 *                              or unspecified, callback will be executed one final time after the last throttled-function call.
 *                              (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset)
 * @param  {Function} callback - A function to be executed after delay milliseconds. The `this` context and all arguments are passed
 *                             through, as-is, to `callback` when the throttled-function is executed.
 * @param  {Boolean} debounceMode - If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms.
 *                                If `debounceMode` is false (at end), schedule `callback` to execute after `delay` ms.
 *
 * @return {Function} A new, throttled, function.
 */
exports.default = addEventListeners;

},{"throttle-debounce/throttle":12}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Helper function to determine the handler type
 * @module  checkLimit
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * check length vs limit and returns  either `0`, `1` or `2`
 * where:
 * - 0 = none
 * - 1 = default
 * - 2 = throttled
 * @type {Function}
 * @const
 * @private
 * @param  {Number} length - length of array
 * @param  {Number} limit - limit of items in array
 * @return {Number} returns a number (one of `[0,1,2]`)
 */
var checkLimit = function checkLimit(length, limit) {
  return !length ? 0 : length > limit ? 2 : 1;
};

exports.default = checkLimit;

},{}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Array of events to reuse
 * @module  EVENTS
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * all events stored for quick access
 * @type {Array}
 * @const
 * @private
 */
var EVENTS = ['scroll', 'resize', 'mousewheel', 'mousemove', 'mouseup', 'touchmove', 'touchend'];

exports.default = EVENTS;

},{}],10:[function(require,module,exports){
'use strict';

var _typeof3 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _typeof2 = typeof Symbol === "function" && _typeof3(Symbol.iterator) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof3(obj);
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof3(obj);
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}(); /**
      * OneListener attempts to gather a collection of handlers to be executed during an event.
      * Usually developers register the handlers directly on the `window` which can cause memory leaks due to unremoved `eventListeners`.
      *
      * When a lot of handlers are active the performance of the app or page might be reduced.
      * One Listener throttles the events globally therefore the handlrers will automatically be throttled.
      * This is based on a handler:limit ratio which can be defined when creating an instance or changed during runtime.
      *
      * @module  one-listener
      *
      * @author  Gregor Adams  <greg@pixelass.com>
      * @author  Jan Nicklas
      * @example
      * import OneListener from 'one-listener'
      * const one = new OneListener({
      *   limit: 6,
      *   throttle: 200
      * })
      * const {requestEventListener, cancelEventListener} = one
      */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 * @author niksy
 * @private
 * @type {Function}
 * @const
 * @param  {Number} delay - A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250
 *                        (or even higher) are most useful.
 * @param  {Boolean} noTrailing - Optional, defaults to false. If noTrailing is true, callback will only execute every
 *                              `delay` milliseconds while the throttled-function is being called. If noTrailing is false
 *                              or unspecified, callback will be executed one final time after the last throttled-function call.
 *                              (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset)
 * @param  {Function} callback - A function to be executed after delay milliseconds. The `this` context and all arguments are passed
 *                             through, as-is, to `callback` when the throttled-function is executed.
 * @param  {Boolean} debounceMode - If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms.
 *                                If `debounceMode` is false (at end), schedule `callback` to execute after `delay` ms.
 *
 * @return {Function} A new, throttled, function.
 */

var _throttle = require('throttle-debounce/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

var _checkLimit2 = require('./check-limit');

var _checkLimit3 = _interopRequireDefault(_checkLimit2);

var _removeEventListeners = require('./remove-event-listeners');

var _removeEventListeners2 = _interopRequireDefault(_removeEventListeners);

var _addEventListeners = require('./add-event-listeners');

var _addEventListeners2 = _interopRequireDefault(_addEventListeners);

var _events = require('./events');

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

/**
 * Creates an instance of OneListener. An instance will handle each event seperately but uses a global throttle and limit.
 * If `scroll` is throttled all requested handlers will return "throttled", while all `resize` handlers rely on their own limit:length ratio.
 * You can create multiple instances to allow adding permanently "throttled" or "unthrottled" `eventListeners` aside of your normal instance.
 *
 * Let's say you have a parallax page which relies on rapid tracking: This would be requested on an unthrottled instance.
 * In case you want to reposition an element on resize it might be wise to request on the throttled instance
 * All handlers are wrapped in a `requestAnimationFrame` to use the browsers default throttling mechanism.
 * @type {Class}
 * @param {Object} options - custom options
 * @param {Number} options.limit - if handlers.length exceeds this value the global eventListener will be throttled
 * @param {Number} options.throttle - if throttled handlers will only be called every `N ms`
 * @return {module:one-listener~OneListener} returns OneListener instance
 *
 * @example
 * // default
 * const one = new OneListener()
 * // custom
 * const one = new OneListener({
 *   limit: 3,
 *   throttle: 500
 * })
 */
var OneListener = function () {
  /**
   * builds and returns an instance of OneListener
   */
  function OneListener(options) {
    _classCallCheck(this, OneListener);

    /**
     * add some options to customize the behavior
     * @type {Object}
     * @private
     * @alias options
     * @memberof module:one-listener~OneListener
     * @property {Number} limit - if handlers.length exceeds this value the global eventListener will be throttled
     * @property {Number} throttle - if throttled handlers will only be called every `N ms`
     */
    this.options = Object.assign({
      limit: 10,
      throttle: 100
    }, options);

    this.handleScroll = this.handleScroll.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.handleMousewheel = this.handleMousewheel.bind(this);
    this.handleMousemove = this.handleMousemove.bind(this);
    this.handleMouseup = this.handleMouseup.bind(this);
    this.handleTouchmove = this.handleTouchmove.bind(this);
    this.handleTouchend = this.handleTouchend.bind(this);
    this.requestEventListener = this.requestEventListener.bind(this);
    this.cancelEventListener = this.cancelEventListener.bind(this);

    this.init();
  }

  /**
   * initialize OneListener.
   * This method is only called to set up to configure
   * the instance.
   * @private
   */

  _createClass(OneListener, [{
    key: 'init',
    value: function init() {
      var _this = this;

      /**
       * global collection of listeners
       * This object is the internal store for the event listeners
       * @type {Object}
       * @private
       * @memberof module:one-listener~OneListener
       * @property {Array} scroll - collection of scroll handlers
       * @property {Array} resize - collection of resize handlers
       * @property {Array} mousewheel - collection of mousewheel handlers
       * @property {Array} mousemove - collection of mousemove handlers
       * @property {Array} mouseup - collection of mouseup handlers
       * @property {Array} touchmove - collection of touchmove handlers
       * @property {Array} touchend - collection of touchend handlers
       */
      this.eventListeners = {
        scroll: [],
        resize: [],
        mousewheel: [],
        mousemove: [],
        mouseup: [],
        touchmove: [],
        touchend: []
      };

      /*
       * the state object will help to check if we need to update the eventListener
       * due to switching from or to a throttled listener.
       * where:
       * - 0 = none
       * - 1 = default
       * - 2 = throttled
       * @private
       * @memberof module:one-listener~OneListener
       * @property {Number} scroll - one of `[0,1,2]`
       * @property {Number} resize - one of `[0,1,2]`
       * @property {Number} mousewheel - one of `[0,1,2]`
       * @property {Number} mousemove - one of `[0,1,2]`
       * @property {Number} mouseup - one of `[0,1,2]`
       * @property {Number} touchmove - one of `[0,1,2]`
       * @property {Number} touchend - one of `[0,1,2]`
       */
      this.state = {
        scroll: 0,
        resize: 0,
        mousewheel: 0,
        mousemove: 0,
        mouseup: 0,
        touchmove: 0,
        touchend: 0
      };

      /**
       * add handlers for all events
       * provides a throttled and default handler
       * @private
       * @memberof module:one-listener~OneListener
       * @property {Object} scroll - scroll handlers
       * @property {Function} scroll.default - default scroll handler
       * @property {Function} scroll.throttled - throttled scroll handler
       * @property {Object} resize - resize handlers
       * @property {Function} resize.default - default resize handler
       * @property {Function} resize.throttled - throttled resize handler
       * @property {Object} mousewheel - mousewheel handlers
       * @property {Function} mousewheel.default - default mousewheel handler
       * @property {Function} mousewheel.throttled - throttled mousewheel handler
       * @property {Object} mousemove - mousemove handlers
       * @property {Function} mousemove.default - default mousemove handler
       * @property {Function} mousemove.throttled - throttled mousemove handler
       * @property {Object} mouseup - mouseup handlers
       * @property {Function} mouseup.default - default mouseup handler
       * @property {Function} mouseup.throttled - default mouseup handler (always unthrottled)
       * @property {Object} touchmove - touchmove handlers
       * @property {Function} touchmove.default - default touchmove handler
       * @property {Function} touchmove.throttled - throttled touchmove handler
       * @property {Object} touchup - touchup handlers
       * @property {Function} touchend.default - default touchend handler
       * @property {Function} touchend.throttled - default touchend handler (always unthrottled)
       */
      this.handlers = {
        scroll: {
          default: this.handleScroll,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleScroll(e);
          })
        },
        resize: {
          default: this.handleResize,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleResize(e);
          })
        },
        mousewheel: {
          default: this.handleMousewheel,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleMousewheel(e);
          })
        },
        mousemove: {
          default: this.handleMousemove,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleMousemove(e);
          })
        },
        mouseup: {
          default: this.handleMouseup,
          // never throttle mouseup
          throttled: this.handleMouseup
        },
        touchmove: {
          default: this.handleTouchmove,
          throttled: (0, _throttle2.default)(this.options.throttle, function (e) {
            return _this.handleTouchmove(e);
          })
        },
        touchend: {
          default: this.handleTouchend,
          // never throttle touchup
          throttled: this.handleTouchend
        }
      };

      this.checkLimitAll(true);
    }

    /**
     * check limit of handlers vs settings and update listeners to throttle when reached
     * @private
     * @param  {String} event - handlers to check
     * @param  {Boolean} force - force update
     */

  }, {
    key: 'checkLimit',
    value: function checkLimit(event, force) {
      var cache = this.state[event];
      // set throttled or default handlers depending on the limit:length ratio
      // if no handlers exist don't add an eventListener
      var update = (0, _checkLimit3.default)(this.eventListeners[event].length, this.options.limit);
      // only apply if the state has changed
      if (update !== cache || force) {
        this.state[event] = update;
        (0, _removeEventListeners2.default)(event, this.handlers[event]);
        switch (update) {
          case 2:
            this.handlers[event] = (0, _addEventListeners2.default)(event, this.handlers[event], this.options.throttle);
            break;
          case 1:
            this.handlers[event] = (0, _addEventListeners2.default)(event, this.handlers[event]);
            break;
          case 0:
            break;
          default:
            throw new Error('state should be on of [0,1,2] but was: ' + this.state[event]);
        }
      }
    }

    /** shortcut to check all handlers that are currently requested
     * @private
     * @param {Boolean} force - forces an update (useful when initializing or similar)
     */

  }, {
    key: 'checkLimitAll',
    value: function checkLimitAll(force) {
      var _this2 = this;

      _events2.default.forEach(function (event) {
        return _this2.checkLimit(event, force);
      });
    }

    /**
     * request an eventListener
     * stores the handlesr in the internal storage and returnst a cancel function
     * @public
     * @param  {String} event - name of the event to request
     * @param  {Function} handler - default eventListener handler
     * @return {Function} returns a function which will cancel the eventListener
     *
     * @example
     * const one = new OneListener()
     * const {requestEventListener} = one
     * // simple listener
     * requestEventListener('scroll', handleScroll)
     * // with cancel
     * const cancelScroll = requestEventListener('scroll', handleScroll)
     * // call cancelEventlistener via cancelScroll()
     * cancelScroll() // listener canceled
     */

  }, {
    key: 'requestEventListener',
    value: function requestEventListener(event, handler) {
      var _this3 = this;

      if (!this.eventListeners.hasOwnProperty(event)) {
        throw new Error('Unkown event ' + event);
      }
      this.eventListeners[event].push(handler);
      this.checkLimit(event);
      return function () {
        return _this3.cancelEventListener(event, handler);
      };
    }

    /**
     * cancels an eventListener
     * looks for the handler and removes it from the list
     * @public
     * @param  {String} event - name of the event to cancel
     * @param  {Function} handler - handler to be removed
     *
     * @example
     * const one = new OneListener()
     * const {cancelEventListener} = one
     * cancelEventListener('scroll', handleScroll)
     */

  }, {
    key: 'cancelEventListener',
    value: function cancelEventListener(event, handler) {
      if (!this.eventListeners.hasOwnProperty(event)) {
        throw new Error('Unkown event ' + event);
      }
      var index = this.eventListeners[event].indexOf(handler);
      // Skip if the handler doesn't exist
      if (index === -1) {
        return;
      }
      // update handlers and rebuild listeners
      this.eventListeners[event].splice(index, 1);
      this.checkLimit(event);
    }

    /**
     * named scroll handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - scroll event
     */

  }, {
    key: 'handleScroll',
    value: function handleScroll(e) {
      this.eventListeners.scroll.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named resize handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - resize event
     */

  }, {
    key: 'handleResize',
    value: function handleResize(e) {
      this.eventListeners.resize.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named mousewheel handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - mousewheel event
     */

  }, {
    key: 'handleMousewheel',
    value: function handleMousewheel(e) {
      this.eventListeners.mousewheel.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named mousemove handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - mousemove event
     */

  }, {
    key: 'handleMousemove',
    value: function handleMousemove(e) {
      this.eventListeners.mousemove.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named mouseup handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers
     * @private
     * @param  {Event} e - mouseup event
     */

  }, {
    key: 'handleMouseup',
    value: function handleMouseup(e) {
      this.eventListeners.mouseup.forEach(function (handler) {
        return handler(e);
      });
    }

    /**
     * named touchmove handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers wrapped in a `requestAnimationFrame`
     * @private
     * @param  {Event} e - touchmove event
     */

  }, {
    key: 'handleTouchmove',
    value: function handleTouchmove(e) {
      this.eventListeners.touchmove.forEach(function (handler) {
        return window.requestAnimationFrame(function () {
          return handler(e);
        });
      });
    }

    /**
     * named touchend handler to use in `addEventListener` and `removeEventListener`
     * calls all handlers
     * @private
     * @param  {Event} e - touchend event
     */

  }, {
    key: 'handleTouchend',
    value: function handleTouchend(e) {
      this.eventListeners.touchend.forEach(function (handler) {
        return handler(e);
      });
    }

    /**
     * return all eventListeners
     * @type {Getter}
     * @alias get debug
     * @name get debug
     * @memberof module:one-listener~OneListener
     * @return {Object} the internal store
     *
     * @example
     * const one = new OneListener()
     * console.log(one.debug)
     */

  }, {
    key: 'debug',
    get: function get() {
      return this.eventListeners;
    }

    /**
     * set limit of handlers
     * @type {Setter}
     * @alias set limit
     * @name set limit
     * @memberof module:one-listener~OneListener
     * @param  {Number} [value] modifies the limit
     *
     * @example
     * const one = new OneListener()
     * one.limit = 4
     */

  }, {
    key: 'limit',
    set: function set(value) {
      if (typeof value === 'number') {
        this.options.limit = value;
        this.checkLimitAll(true);
      } else {
        throw new Error('value should be of type "number" instead got ' + (typeof value === 'undefined' ? 'undefined' : _typeof(value)));
      }
    }

    /**
     * get limit of handlers
     * @type {Getter}
     * @alias get limit
     * @name get limit
     * @memberof module:one-listener~OneListener
     * @return {Number} returns the current limit
     *
     * @example
     * const one = new OneListener()
     * let limit = one.limit
     */

    , get: function get() {
      return this.options.limit;
    }

    /**
     * set throttle of handlers
     * @type {Setter}
     * @alias set throttle
     * @name set throttle
     * @memberof module:one-listener~OneListener
     * @param  {Number} [value] modifies the throttle
     *
     * @example
     * const one = new OneListener()
     * one.throttle = 500
     */

  }, {
    key: 'throttle',
    set: function set(value) {
      if (typeof value === 'number') {
        this.options.throttle = value;
        this.checkLimitAll(true);
      } else {
        throw new Error('value should be of type "number" instead got ' + (typeof value === 'undefined' ? 'undefined' : _typeof(value)));
      }
    }

    /**
    * get throttle of handlers
    * @type {Getter}
    * @alias get throttle
    * @name get throttle
    * @memberof module:one-listener~OneListener
    * @return {Number} returns the current throttle
    *
    * @example
    * const one = new OneListener()
    * let throttle = one.throttle
    */

    , get: function get() {
      return this.options.throttle;
    }
  }]);

  return OneListener;
}();

exports.default = OneListener;

},{"./add-event-listeners":7,"./check-limit":8,"./events":9,"./remove-event-listeners":11,"throttle-debounce/throttle":12}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Helper function to remove eventListeners from `window`
 * @module  removeEventListeners
 * @private
 * @author  Gregor Adams  <greg@pixelass.com>
 */

/**
 * remove all listeners that are potentially on `window`
 * @type {Function}
 * @const
 * @private
 * @param  {String} event   event to forget
 * @param  {Object} handler - the handler object containing the throttled and unthrottled handlers
 * @param  {Function} handler.default - default handler
 * @param  {Function} handler.throttled - throttled handler
 */
var removeEventListeners = function removeEventListeners(event, handler) {
  window.removeEventListener(event, handler.throttled);
  window.removeEventListener(event, handler.default);
};

exports.default = removeEventListeners;

},{}],12:[function(require,module,exports){
/* eslint-disable no-undefined,no-param-reassign,no-shadow */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 *
 * @param  {Number}    delay          A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
 * @param  {Boolean}   noTrailing     Optional, defaults to false. If noTrailing is true, callback will only execute every `delay` milliseconds while the
 *                                    throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
 *                                    after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
 *                                    the internal counter is reset)
 * @param  {Function}  callback       A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
 *                                    to `callback` when the throttled-function is executed.
 * @param  {Boolean}   debounceMode   If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms. If `debounceMode` is false (at end),
 *                                    schedule `callback` to execute after `delay` ms.
 *
 * @return {Function}  A new, throttled, function.
 */
module.exports = function ( delay, noTrailing, callback, debounceMode ) {

	// After wrapper has stopped being called, this timeout ensures that
	// `callback` is executed at the proper times in `throttle` and `end`
	// debounce modes.
	var timeoutID;

	// Keep track of the last time `callback` was executed.
	var lastExec = 0;

	// `noTrailing` defaults to falsy.
	if ( typeof noTrailing !== 'boolean' ) {
		debounceMode = callback;
		callback = noTrailing;
		noTrailing = undefined;
	}

	// The `wrapper` function encapsulates all of the throttling / debouncing
	// functionality and when executed will limit the rate at which `callback`
	// is executed.
	function wrapper () {

		var self = this;
		var elapsed = Number(new Date()) - lastExec;
		var args = arguments;

		// Execute `callback` and update the `lastExec` timestamp.
		function exec () {
			lastExec = Number(new Date());
			callback.apply(self, args);
		}

		// If `debounceMode` is true (at begin) this is used to clear the flag
		// to allow future `callback` executions.
		function clear () {
			timeoutID = undefined;
		}

		if ( debounceMode && !timeoutID ) {
			// Since `wrapper` is being called for the first time and
			// `debounceMode` is true (at begin), execute `callback`.
			exec();
		}

		// Clear any existing timeout.
		if ( timeoutID ) {
			clearTimeout(timeoutID);
		}

		if ( debounceMode === undefined && elapsed > delay ) {
			// In throttle mode, if `delay` time has been exceeded, execute
			// `callback`.
			exec();

		} else if ( noTrailing !== true ) {
			// In trailing throttle mode, since `delay` time has not been
			// exceeded, schedule `callback` to execute `delay` ms after most
			// recent execution.
			//
			// If `debounceMode` is true (at begin), schedule `clear` to execute
			// after `delay` ms.
			//
			// If `debounceMode` is false (at end), schedule `callback` to
			// execute after `delay` ms.
			timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
		}

	}

	// Return the wrapper function.
	return wrapper;

};

},{}]},{},[1]);
