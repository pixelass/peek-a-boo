# PEEK A BOO

show elements when they enter the viewport

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://raw.githubusercontent.com/pixelass/peek-a-boo/master/LICENSE)
[![GitHub issues](https://img.shields.io/github/issues/pixelass/peek-a-boo.svg?style=flat-square)](https://github.com/pixelass/peek-a-boo/issues)
[![Coveralls](https://img.shields.io/coveralls/pixelass/peek-a-boo.svg?style=flat-square)](https://coveralls.io/github/pixelass/peek-a-boo)
[![bitHound](https://img.shields.io/bithound/code/github/pixelass/peek-a-boo.svg?style=flat-square)](https://www.bithound.io/github/pixelass/peek-a-boo)
[![bitHound](https://img.shields.io/bithound/devDependencies/github/pixelass/peek-a-boo.svg?style=flat-square)](https://www.bithound.io/github/pixelass/peek-a-boo)

[![Browserify](https://img.shields.io/badge/build-browserify-3c6991.svg?style=flat-square)](http://browserify.org/)
[![Babel](https://img.shields.io/badge/babel-stage--0-f5da55.svg?style=flat-square)](http://babeljs.io/docs/plugins/preset-stage-0/)
[![code style xo](https://img.shields.io/badge/code_style-XO-64d8c7.svg?style=flat-square)](https://github.com/sindresorhus/xo)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-44aa44.svg?style=flat-square)](https://github.com/conventional-changelog/standard-version)
[![test ava](https://img.shields.io/badge/test-🚀_AVA-0e1d5c.svg?style=flat-square)](https://github.com/avajs/ava)

[![yarn](https://img.shields.io/badge/yarn-friendly-2c8ebb.svg?style=flat-square)](https://yarnpkg.com/)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-44aa44.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)

<!-- toc -->

- [Links](#links)
- [Usage](#usage)
  * [Vanilla JS](#vanilla-js)
  * [Custom classNames](#custom-classnames)
  * [Preload nested images](#preload-nested-images)
  * [Add offset](#add-offset)
  * [Preload all images](#preload-all-images)
  * [jQuery](#jquery)
- [Developing](#developing)

<!-- tocstop -->

## Links
* [Documentation](https://pixelass.github.io/peek-a-boo/api/)
* [Vanilla JS Demo](https://pixelass.github.io/peek-a-boo/)
* [jQuery Demo](https://pixelass.github.io/peek-a-boo/jquery.html)

## Usage

```shell
yarn add peek-a-boo
```

### Vanilla JS

```js
import Peekaboo from 'peek-a-boo'

const elements = Array.from(document.querySelectorAll('.peekaboo'))

elements.forEach(el => {
  const peek = new Peekaboo()
  peek.init(el)
})
```

### Custom classNames

This example uses custom classNames (via css-modules import)

```js
import Peekaboo from 'peek-a-boo'
import styles from './main.css'

const elements = Array.from(document.querySelectorAll('.peekaboo'))

elements.forEach(el => {
  // configure instance
  const peek = new Peekaboo({
    classNames: {
      isLoading: styles.loading,
      hasError: styles.error,
      isLoaded: styles.loaded,
      isOnscreen: styles.onscreen,
      isOffscreen: styles.offscreen,
      isAnimating: styles.animating,
      isDone: styles.done,
      initiallyVisible: styles.visible
    }
  })
  // initialize instance
  peek.init(el)
})
```

### Preload nested images

This example preloads nested images

```js
import Peekaboo from 'peek-a-boo'

const elements = Array.from(document.querySelectorAll('.peekaboo'))

elements.forEach(el => {
  const peek = new Peekaboo({
    preload: true
  })
  peek.init(el)
})
```

### Add offset
0
This example shows elements when they entered 250px or more from the bottom of the viewport.

```js
import Peekaboo from 'peek-a-boo'

const elements = Array.from(document.querySelectorAll('.peekaboo'))

elements.forEach(el => {
  const peek = new Peekaboo({
    offset: 250
  })
  peek.init(el)
})
```

### Preload all images

This example preloads all images by wrapping the call in a preloader.

```js
import Peekaboo from 'peek-a-boo'
import {loadImages} from 'peek-a-boo/helpers'

const images = Array.from(document.querySelectorAll('.peekaboo img')).map(el => el.src)
const elements = Array.from(document.querySelectorAll('.peekaboo'))

loadImages(images).then(() => {
  elements.forEach(el => {
    const peek = new Peekaboo()
    peek.init(el)
  })
})
```

### jQuery

This example uses jQuery

```js
import $ from 'jquery'
import 'peek-a-boo/jquery'

$('.peekaboo').Peekaboo()
```

### Methods

## Developing

To start a dev server and start developing try the following commands

* `start`: starts the dev server and builds the required files
* `test`: runs test and lints files
* `run dev`: starts the dev server and watches the required files
* `run babel`: generates lib from source
* `run build`: builds all files from source
* `run watch`: builds and watches all files from source
* `run lint`: lints javascript files
* `run release`: release new version using "standard-version"

