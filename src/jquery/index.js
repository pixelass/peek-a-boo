/**
 * A jQuery wrapper for peek-a-boo
 * @file src/jquery/index.js
 * @module jQuery.Peekaboo
 * @author Gregor Adams <greg@pixelass.com>
 * @example
 * // initialize plugin instances
 * // Will create an array of Jquery plugin instances
 * $('.peekaboo').Peekaboo()
 *
 * // Options can be passed as an object.
 * // more information can be found0 in `src/index.js`
 * $('.peekaboo').Peekaboo({
 *   classNames: {
 *    isLoading: styles.loading,
 *    hasError: styles.error,
 *    isLoaded: styles.loaded,
 *    isOnscreen: styles.onscreen,
 *    isOffscreen: styles.offscreen,
 *    isAnimating: styles.animating,
 *    isDone: styles.done,
 *    initiallyVisible: styles.visible
 *  }
 * })
 *
 * // Instances can be destroyed seperately.
 * // To destroy all instances you can use `jQuery.each`
 * const $peeks = $('.peekaboo').Peekaboo()
 * $peeks.each((index, $peek) => $peek.destroy())
 */

import Plugin, {PLUGIN_NAME} from '../'
import createWrapper from './wrapper'

createWrapper(Plugin, PLUGIN_NAME)
