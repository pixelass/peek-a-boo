/**
 * A jQuery wrapper for vanilla JS modules
 * @private
 * @file src/jquery/wrapper.js
 * @module createWrapper
 * @author Gregor Adams <greg@pixelass.com>
 */

import $ from 'jquery'

/**
 * jQuery wrapper
 * @private
 * @param {function} Plugin
 * @param {string} pluginName
 */
const createWrapper = (Plugin, pluginName) => {
  $.fn[pluginName] = function (options) {
    return this.map((i, el) => {
      const instance = new Plugin(options)
      instance.init(el)
      return $.data(el, pluginName, instance)
    })
  }
}

export default createWrapper
