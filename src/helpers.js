/**
 * Small helper library. Includes image preloaders and viewport checks
 * @file src/heplers.js
 * @module helpers
 * @author Gregor Adams <greg@pixelass.com>
 */

/**
 * Load image with a promise
 * @param {string} src
 * @returns {promise}
 */
const loadImage = src => new Promise((resolve, reject) => {
  if (typeof src === 'string') {
    const img = global.document.createElement('img')
    img.src = src
    img.onload = e => resolve(e.target)
    img.onerror = () => resolve(null)
    return img
  }
  return reject(new Error(`Expected src to be of type "string". instead got ${typeof src}`))
})

/**
 * Loads a set of images with a promise
 * @param {array.<string>} arr
 * @returns {promise}
 */
const loadImages = sources => Promise.all(sources.map(src => loadImage(src)))

/**
 * Check if an element has entered the page from the bottom.
 * Respects offset if set. The offset will define how far the image has entered the screen.
 * @param {HTMLElement} el
 * @param {number} offset Number of pixels the image must have entered the viewport.
 *                        Can be a positive or negative number.
 * @returns {boolean} returns true if the image is visible
 */
const inViewport = (element, offset = 0) => {
  var {top} = element.getBoundingClientRect()
  const offsetBottom = top - global.innerHeight + offset
  return offsetBottom < 0
}

export {
  loadImage,
  loadImages,
  inViewport
}
