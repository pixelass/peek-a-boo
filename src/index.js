/**
 * @file src/index.js
 * @author Gregor Adams <greg@pixelass.com>
 * @module Peekaboo
 * @example
 * // import module from node_modules
 * import Peekaboo from 'peek-a-boo'
 *
 * // Get all elements as an array
 * const elements = Array.from(document.querySelectorAll('.peekaboo'))
 *
 * // Create an Array of initialized instances
 * const peeks = elements.map(el => {
 *   const peek = new Peekaboo()
 *   peek.init(el)
 *   return peek
 * })
 *
 * // Create an Array of initialized instances,
 * // with custom configurationconfiguration
 * const peeks = elements.map(el => {
 *   // configure instance
 *   const peek = new Peekaboo({
 *     classNames: {
 *       isLoading: styles.loading,
 *       hasError: styles.error,
 *       isLoaded: styles.loaded,
 *       isOnscreen: styles.onscreen,
 *       isOffscreen: styles.offscreen,
 *       isAnimating: styles.animating,
 *       isDone: styles.done,
 *       initiallyVisible: styles.visible
 *     }
 *   })
 *   // initialize instance
 *   peek.init(el)
 *   return peek
 * })
 *
 * // Destroy instances.
 * // create instance (with settings)
 * const peek = new Peekaboo()
 * // Initialize instance.
 * peek.init(document.querySelector('.peekaboo'))
 * // Destroy instance.
 * peek.destroy()
 */

import {requestEventListener} from './event-listeners'

import {
  loadImages,
  inViewport
} from './helpers'

/**
 * Global name of the jQuery plugin. This is to call the plugin.
 * @memberof module:Peekaboo
 * @type {string}
 */
const PLUGIN_NAME = 'Peekaboo'

/**
 * Default options for plugin instances
 * @memberof module:Peekaboo
 * @type {object}
 * @prop {object.<object>} classNames
 * @prop {string} classNames.pluginLoaded Initially set (exists until destroyed).
 * @prop {string} classNames.isLoading Initially set (while image is loading).
 * @prop {string} classNames.hasLoaded Nested images have been loaded.
 * @prop {string} classNames.hasError Nested images have an error.
 * @prop {string} classNames.isOnscreen Image has been on screen.
 * @prop {string} classNames.isOffscreen Image has not been on screen.
 * @prop {string} classNames.isAnimating Animations are currently running.
 * @prop {string} classNames.isDone Element is done.
 * @prop {string} classNames.initiallyVisible Element is initially visible.
 * @prop {boolean} preload
 * @prop {array.<string>} events A list of events to listen for | (resize, scroll, orientationchange...)
 * @prop {number} offset
 */
const PLUGIN_DEFAULTS = {
  classNames: {
    isLoading: 'loading',
    hasLoaded: 'loaded',
    hasError: 'error',
    isOnscreen: 'onscreen',
    isOffscreen: 'offscreen',
    isAnimating: 'animating',
    isDone: 'done',
    initiallyVisible: 'visible',
    pluginLoaded: 'peekabooLoaded'
  },
  preload: false,
  delay: false,
  offset: 150,
  events: ['scroll', 'resize']
}

class Peekaboo {
  /**
   * Peekaboo is a plugin to change appearance of elements,
   * depending on their position in the viewport.
   * @memberof module:Peekaboo
   * @param {object} [options={}]
   * @returns {this}
   */
  constructor(options = {}) {
    this.options = {...PLUGIN_DEFAULTS, ...options}
    this.addAnimationClasses = this.addAnimationClasses.bind(this)
    this.removeAnimationClasses = this.removeAnimationClasses.bind(this)
    return this.methods
  }

  /**
   * Public methods.
   * @memberof module:Peekaboo
   * @returns {object}
   */
  get methods() {
    return {
      init: this.init.bind(this),
      destroy: this.destroy.bind(this)
    }
  }

  /**
   * Initialize plugin
   * @memberof module:Peekaboo
   * @param {HTMLElement} el
   */
  init(el) {
    const {pluginLoaded} = this.options.classNames
    this.el = el
    el.classList.add(pluginLoaded)
    const nestedImages = Array.from(el.querySelectorAll('img'))
    if (this.options.preload) {
      loadImages(nestedImages.map(el => el.src))
        .then(() => this.onInit(el))
        .catch(err => {
          throw err
        })
    } else {
      this.onInit(el)
    }
  }

  /**
   * Destroys the instance. Reverts all changes that have been made to the DOM.
   * @memberof module:Peekaboo
   * Also removes all event Listeners.
   * Removes all event listeners and element class names.
   */
  destroy() {
    // get all classNames from the options and put them in an array,
    // to allow batch removal
    const {classNames} = this.options
    const classes = Object.keys(classNames).map(key => classNames[key])
    // remove all global event listeners
    this.options.events.forEach(event => {
      this[`cancel_${event}`]()
    })
    // Remove all event listeners on the element
    this.el.removeEventListener('animationstart', this.addAnimationClasses)
    this.el.removeEventListener('animationend', this.removeAnimationClasses)
    // Then remove all classNames
    this.el.classList.remove(...classes)
  }

  /**
   * Callback ofter plugin instance has been initialized
   * @memberof module:Peekaboo
   * @private
   * @param {HTMLElement} el
   */
  onInit(el) {
    const {hasLoaded, isLoading} = this.options.classNames
    // Create namespaced eventnames
    const initiallyVisible = inViewport(el, this.options.offset)
    this.initClasses(el, initiallyVisible)

    // Add global event listeners for a set of events
    this.options.events.forEach(event => {
      this[`cancel_${event}`] = requestEventListener(event, () => {
        this.checkForItems(el)
          .then(() => {
            this[`cancel_${event}`]()
          })
          .catch(err => err)
      })
    })

    // Add eventlisteners for the animation once
    // When done add a class as a flag

    el.addEventListener('animationstart', this.addAnimationClasses, {
      passive: true,
      once: true
    })
    el.addEventListener('animationend', this.removeAnimationClasses, {
      passive: true,
      once: true
    })
    // Images are loaded.
    // Toggle the classes
    el.classList.add(hasLoaded)
    el.classList.remove(isLoading)
  }

  /**
   * Adds animation classes to instance.
   * @memberof module:Peekaboo
   * @private
   */
  addAnimationClasses() {
    const {isAnimating} = this.options.classNames
    this.el.classList.add(isAnimating)
  }

  /**
   * Removes animation classes from instance and sets "done" classes
   * @private
   */
  removeAnimationClasses() {
    const {isAnimating, isDone} = this.options.classNames
    this.el.classList.remove(isAnimating)
    this.el.classList.add(isDone)
  }

  /**
   * Handles initially needed classNames on an element
   * @memberof module:Peekaboo
   * @private
   * @param {HTMLElement} el
   * @param {boolean} visible
   */
  initClasses(element, visible) {
    const {isLoading, initiallyVisible} = this.options.classNames
    element.classList.add(isLoading)
    element.classList.toggle(initiallyVisible, visible)
    this.currentClasses(element, visible)
  }

  /**
   * add live classNames. Adds `isOnscreen` or `isOffscreen` class depending on the visibility
   * @memberof module:Peekaboo
   * @private
   * @param {HTMLElement} el
   * @param {boolean} visible
   */
  currentClasses(element, visible) {
    const {isOffscreen, isOnscreen} = this.options.classNames
    element.classList.toggle(isOnscreen, visible)
    element.classList.toggle(isOffscreen, !visible)
  }

  /**
   * Checks if an item matches the required criteria to be visible
   * @memberof module:Peekaboo
   * @private
   * @param {HTMLElement} el
   * @returns {promise}
   */
  checkForItems(el) {
    return new Promise((resolve, reject) => {
      const isVisible = inViewport(el, this.options.offset)
      this.currentClasses(el, isVisible)
      if (isVisible) {
        resolve(el)
      } else {
        reject(new Error('Element not visible'))
      }
    })
  }
}

export {PLUGIN_NAME}
export default Peekaboo
